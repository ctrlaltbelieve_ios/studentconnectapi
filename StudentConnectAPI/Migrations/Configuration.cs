namespace StudentConnectAPI.Migrations
{
    using StudentConnectAPI.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<StudentConnectAPI.Models.StudentConnectAPIContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(StudentConnectAPI.Models.StudentConnectAPIContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            List<User> users = new List<User>()
            {
                new User { Name = "John Doe", Email = "johnDoe@bcit.ca", password="p@$$w0rd" },
                new User { Name = "Jane Doe", Email = "janeDoe@bcit.ca", password = "p@$$w0rd" },
                new User { Name = "Mike Johnson", Email = "mj@bcit.ca", password = "p@$$w0rd" }        
            };


            context.Users.AddOrUpdate(u => u.UserId,
                users.ToArray()
                );

            context.Groups.AddOrUpdate(g => g.GroupId,
                            new Group { Name = "COMP 4976", Description ="ASP.NET Web Development", Owner= 3, Users=  new List<User>(users)},
                            new Group { Name = "COMP 4977", Description = "iOS Development", Owner = 3, Users = new List<User>(users.Skip(1).Take(1)) },
                            new Group { Name = "COMP 4711", Description = "Code Igniter", Owner = 3, Users = new List<User>(users.Take(1)) },
                            new Group { Name = "BLAW 3600", Description = "Business Law", Owner = 3, Users = new List<User>(users.Take(3)) },
                            new Group { Name = "COMP 4735", Description = "Operating Systems", Owner = 3, Users = new List<User>(users.Take(1)) }
                            );

            context.Announcements.AddOrUpdate(a => a.AnnouncementId,
                            new Announcement { Message="Finals next week",  Sent= DateTime.Now , GroupID = 1  },
                            new Announcement { Message = "Assignment 3 due", Sent = DateTime.Now, GroupID = 3 }
                            );

            context.UserGroups.AddOrUpdate(d => d.UserGroupId,
                            new UserGroup { User = 1, Group = 3},
                            new UserGroup { User = 2, Group = 5 }
                            );
        }
    }
}
