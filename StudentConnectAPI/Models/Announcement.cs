﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StudentConnectAPI.Models
{
    public class Announcement
    {
        [Key]
        public int AnnouncementId { get; set; }

        public int GroupID { get; set; }

        [Required]
        public string Message { get; set; }

        public DateTime Sent { get; set; }

    }
}