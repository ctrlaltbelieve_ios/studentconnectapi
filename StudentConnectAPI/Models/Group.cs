﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StudentConnectAPI.Models
{
    public class Group
    {
        public int GroupId { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public int Owner { get; set; }

        public ICollection<User> Users { get; set; }

    }
}