﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentConnectAPI.Models
{
    public class UserGroup
    {
        public int UserGroupId { get; set; }
        public int Group { get; set; }
        public int User { get; set; }
    }
}